import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';

@Directive({
  selector: '[appRandom]'
})
export class RandomDirective {

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef
  ) { }

  @Input()
  set appRandom(value: string) {
    if (Math.random() > 0.5) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    }
  }
}
