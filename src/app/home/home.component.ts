import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @Input() title2 = '';
  @Output() firstRun = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.firstRun.emit(this.title2);
  }

}
