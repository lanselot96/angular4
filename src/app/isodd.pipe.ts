import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isodd'
})
export class IsoddPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.filter(m => {return m % 2});
  }

}
