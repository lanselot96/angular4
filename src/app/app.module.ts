import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { FormsModule} from "@angular/forms";
import { HomeComponent } from './home/home.component';
import { RandomDirective } from './random.directive';
import { IsoddPipe } from './isodd.pipe';
import { NotFountComponent } from './not-fount/not-fount.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    RandomDirective,
    IsoddPipe,
    NotFountComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  child_title;
  constructor(){

  }
  onFirstRun(){
    this.child_title = '';
  }
}
