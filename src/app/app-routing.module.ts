import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent} from "./app.component";
import {HomeComponent} from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { NotFountComponent } from "./not-fount/not-fount.component";

const routes: Routes = [
  // {
  //   path: 'home',
  //   children: [
  //     {
  //       path: '',
  //       component: HomeComponent
  //     },{
  //       path: 'about2',
  //       component: AboutComponent
  //     },{
  //       path: '**',
  //       component: AboutComponent
  //     }
  //   ]
  // },{
  //   path: 'about',
  //   component: AboutComponent
  // },{
  //   path: '**',
  //   component: NotFountComponent
  // }
  {
    path: '',
    component: AppComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
